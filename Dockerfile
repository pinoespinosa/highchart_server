FROM node:12


COPY . .

EXPOSE 8080

CMD [ "node", "app/bin/cli.js", "--enableServer", "1"  ]